import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import argparse
import path
from os.path import basename
import os
from datetime import datetime

class MyHandler(FileSystemEventHandler):
    
    pattern = ""
    delay = 10
    bat_file = ""
    Dtime = None

    def __init__(self, delay, bat_file, pattern):
        self.pattern = pattern;
        self.delay = delay;
        self.bat_file = bat_file;
        self.Dtime = datetime.now()

    def on_modified(self, event):
        if self.pattern in event.src_path:
            print("File Changed")
            self.run()
    
    def run(self):
        if (self.Dtime == None or datetime.now() - self.Dtime).total_seconds() > self.delay:
            print("Running Script")
            os.system(self.bat_file)
            self.Dtime = datetime.now()
        else:
            print("Waiting...")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', dest = 'FILE', help='File to watch')
    parser.add_argument('-b', dest = 'BAT', help='Bat to run')
    parser.add_argument('-d', dest = 'DELAY', default=10, help='delay')
    args = parser.parse_args()

    original_filename = basename(args.FILE)
    abs_path_withouth_name = args.FILE.replace(original_filename, "")

    event_handler = MyHandler(int(args.DELAY), args.BAT, original_filename)
    observer = Observer()
    observer.schedule(event_handler, 
        path=".{}".format(abs_path_withouth_name), recursive=False)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
