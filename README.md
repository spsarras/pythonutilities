# PythonUtilities

## FileWatcherRunBat

This is a script to look for file changes and for every modification run a user defined .bat script. One usage is to create a build file to be run every time you save your source. For multiple files this code can be changed to watch changes on a directory.

###Requirements

- argparse
- watchdog

###Usage

```
python FileWatcherRunBat.py -f <C:\dir\fileToWatch.md> -b <C:\dir\to\batfile.bat> -d <delay in seconds>
```

## GMusicPlaylistPerFolder

This is a script to automatically create playlists from your music folder structure. This script assumes you have uploaded your music in google music, and that you have organised your playlists using folders. The problem is that google music doesn't retain any folder structure when uploading music, this is where this script comes into play. After the upload process has finished use this to query your library and create playlists based on your local files.

###Requirements

- [gmusicapi](https://github.com/simon-weber/gmusicapi)
- mutagen
- argparse

###Usage

```
python createPlaylists.py <C:dir/root> 
```