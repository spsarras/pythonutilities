#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division, absolute_import, unicode_literals
from builtins import *  # noqa

from getpass import getpass

from gmusicapi import Mobileclient


def ask_for_credentials():
	"""Make an instance of the api and attempts to login with it.
	Return the authenticated api.
	"""

	# We're not going to upload anything, so the Mobileclient is what we want.
	api = Mobileclient()

	logged_in = False
	attempts = 0

	while not logged_in and attempts < 3:
		email = input('Email: ')
		password = getpass()

		logged_in = api.login(email, password, Mobileclient.FROM_MAC_ADDRESS)
		attempts += 1

	return api

def stam_credentials():
	"""Make an instance of the api and attempts to login with it.
	Return the authenticated api.
	"""

	# We're not going to upload anything, so the Mobileclient is what we want.
	api = Mobileclient()

	logged_in = False
	attempts = 0

	while not logged_in and attempts < 3:
		#email = input('Email: ')
		email = 'psarras.st@gmail.com'
		password = 'OEDB#5c0rpi0nl5r@17'#getpass()

		logged_in = api.login(email, password, Mobileclient.FROM_MAC_ADDRESS)
		attempts += 1

	return api

def demonstrate():
	"""Demonstrate some api features."""

	api = stam_credentials()

	if not api.is_authenticated():
		print("Sorry, those credentials weren't accepted.")
		return

	print('Successfully logged in.')
	print()

	# Get all of the users songs.
	# library is a big list of dictionaries, each of which contains a single song.
	print('Loading library...', end=' ')
	library = api.get_all_songs()
	print('done.')

	print(len(library), 'tracks detected.')
	print()

	# Show some info about a song. There is no guaranteed order;
	# this is essentially a random song.
	first_song = library[0]
	print("The first song I see is '{}' by '{}'.".format(
		first_song['title'], first_song['artist']))
	print(first_song)

	for songs in library:
		if songs['title'] == '01. Music (George Miles Cover) - IVORY TOWER.mp3':
			print('found: {}'.format(songs['title']))

	# We're going to create a new playlist and add a song to it.
	# Songs are uniquely identified by 'song ids', so let's get the id:

	song_id = first_song['id']

	print("I'm going to make a new playlist and add that song to it.")
	print("I'll delete it when we're finished.")
	print()
	playlist_name = input('Enter a name for the playlist: ')

	# Like songs, playlists have unique ids.
	# Google Music allows more than one playlist of the same name;
	# these ids are necessary.
	playlist_id = api.create_playlist(playlist_name)
	print('Made the playlist.')
	print()

	# Now let's add the song to the playlist, using their ids:
	api.add_songs_to_playlist(playlist_id, song_id)
	print('Added the song to the playlist.')
	print()

	# We're all done! The user can now go and see that the playlist is there.
	# The web client syncs our changes in real time.
	input('You can now check on Google Music that the playlist exists.\n'
		  'When done, press enter to delete the playlist:')
	api.delete_playlist(playlist_id)
	print('Deleted the playlist.')

	# It's good practice to logout when finished.
	api.logout()
	print('All done!')


import argparse
import glob
import os
from mutagen.easyid3 import EasyID3
import linecache
import sys

def PrintException():
	exc_type, exc_obj, tb = sys.exc_info()
	f = tb.tb_frame
	lineno = tb.tb_lineno
	filename = f.f_code.co_filename
	linecache.checkcache(filename)
	line = linecache.getline(filename, lineno, f.f_globals)
	print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

#folder = 'D:\\MUSIC\\Youtube_Mix_1'

def create_playlist(api, playlist_name):
	playlist_id = api.create_playlist(playlist_name)
	return playlist_id

def get_playlist(playlists, name):
	if any(playlist['name'] == name for playlist in playlists):
		return playlist['id']

if __name__ == '__main__':
	print("Started")
	parser = argparse.ArgumentParser()
	parser.add_argument(dest='root', default='', help='root folder to start creating playlists from')
	parser.add_argument("-existing", dest="ignore_existing", action='store_true', help=
						"ignore existing playlists")
	parser.add_argument("-complete", dest="ignore_complete", action='store_true', help=
						"ignore complete playlists")
	parser.add_argument("-debug", dest="debug", action='store_true', help=
						"extra info")
	args = parser.parse_args()

	folder = args.root

	#Login
	print('Logging in', end=' ')
	api = stam_credentials()
	print('done')

	if not api.is_authenticated():
		print("Sorry, those credentials weren't accepted.")
	else:
		print('Loading library...', end=' ')
		library = api.get_all_songs()
		print('done')
		print('Loading Playlists...', end=' ')
		playlists = api.get_all_playlists()
		print('done')
		
		FolderCounter = 0

		try:
			tree = os.walk(folder)
			for subdir, dirs, files in tree:
				songs = glob.glob(os.path.join(subdir, "*.mp3"))#subdir + "\\*.mp3")
				folderName = os.path.basename(subdir)
				FolderCounter += 1
				print("Processing Folder {2}: {0} with {1} songs..."
					.format(folderName, len(songs),FolderCounter, end=' '))
				if args.debug:
					print("Started")
				exists = False
				if len(songs) > 0:
					#Playlists
					playlistsMatch = [playlist for playlist in playlists if playlist['name'] == folderName]
					if not len(playlistsMatch) > 0:
						if args.debug:
							print('Creating new Playlist')
						playlist_id = create_playlist(api, folderName)
						playlists = api.get_all_playlists()
						currentPlaylist = [playlist for playlist in playlists if playlist['name'] == folderName][0]
					else:
						if args.debug:
							print('playlist Exists')
						currentPlaylist = playlistsMatch[0]
						playlist_id = currentPlaylist['id']
						exists = True
						#exists
					
					if args.debug:
						print("Got playlist {1} with id: {0}".format(playlist_id, folderName))

					ContentsPlaylists = api.get_all_user_playlist_contents()
					matchingPlaylists = [c for c in ContentsPlaylists if c['id'] == playlist_id]
					contents = []
					if len(matchingPlaylists) > 0:
						contents = matchingPlaylists[0]['tracks']
					
					if args.debug:
						print("Current contents: {0}".format(len(contents)))

					addSongs = True
					if args.ignore_existing and exists:
						addSongs = False
					if args.ignore_complete and len(contents) == len(songs):
						addSongs = False

					#Songs
					if addSongs:
						counter = 0
						songs_to_add = []
						for song in songs:
							audio = EasyID3(song)
							songName = os.path.basename(song)
							if len(audio) > 0 and 'title' in audio:
								songName = audio['title'][0]
								if args.debug:
									print("Using Title Tag")
							if args.debug:
								print("Trying to add {0}".format(songName))

							songsMatching = [s for s in library if s['title'] == songName]
							if(len(songsMatching) > 0):

								if not any(c for c in contents if c['trackId'] == songsMatching[0]['id']):
									songs_to_add.append(songsMatching[0]['id'])
									counter += 1
									if args.debug:
										print("Added song {0} at {1}".format(songName, folderName))
								else:
									if args.debug:
										print("Song Already Exists in the playlist")
									pass
							else:
								if args.debug:
									print("Couldn't find {0} in the library".format(songName))
								pass
						
						if(len(songs_to_add) > 0):
							print("Adding Songs...", end=' ')
							api.add_songs_to_playlist(playlist_id, songs_to_add)
							print("Done!")
							print("Successfully added {0}/{1} songs in playlist".format(counter, len(songs)))
						else:
							print("Skip!")
					else:
						print("Ignore Playlist!")
				else:
					print("No Songs in Folder")
				print("")
		except KeyboardInterrupt:
			print("Logging out!")
			api.logout()			
		except Exception as e:
			PrintException()
			#print(e)
			print("Logging out!")
			api.logout()

	#demonstrate()